<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="entry-thumbnail">
		<?php thienpham_thumbnail('thumbnail'); ?>
	</div>
	<div class="entry-header">
		<?php thienpham_entry_header(); ?>
		<?php thienpham_entry_meta(); ?>
	</div>
	<div class="entry-content">
		<?php thienpham_entry_content(); ?>
		<?php ( is_single() ? thienpham_entry_tag() : '' ); ?>
	</div>
</article>