<?php
function thienpham_plugin_activation() {
 
        // Khai bao plugin can cai dat
        // $plugins = array(
        //         array(
        //                 'name' => 'Redux Framework',
        //                 'slug' => 'redux-framework',
        //                 'required' => true
        //         )
        // );

        $plugins = array(
                array(
                        'name' => 'Duplicator',
                        'slug' => 'duplicator',
                        'required' => true
                ),
                array(
                        'name' => 'Advanced Custom Fields',
                        'slug' => 'advanced-custom-fields',
                        'required' => true
                ),
                array(
                        'name' => 'TinyMCE Advanced',
                        'slug' => 'tinymce-advanced',
                        'required' => true
                ),
                array(
                        'name' => 'Yoast SEO',
                        'slug' => 'wordpress-seo',
                        'required' => true
                ),
                array(
                        'name' => 'Custom Post Type UI',
                        'slug' => 'custom-post-type-ui',
                        'required' => true
                ),
                array(
                        'name' => 'Admin Columns',
                        'slug' => 'codepress-admin-columns',
                        'required' => true
                ),
               
                array(
                        'name' => 'Page Builder by SiteOrigin',
                        'slug' => 'siteorigin-panels',
                        'required' => true
                ),
                array(
                        'name' => 'SiteOrigin Widgets Bundle',
                        'slug' => 'so-widgets-bundle',
                        'required' => true
                ),
                array(
                        'name' => 'Ultimate Addons for SiteOrigin',
                        'slug' => 'addon-so-widgets-bundle',
                        'required' => true
                )
        );
 
        // Thiet lap TGM
        $configs = array(
                'menu' => 'tp_plugin_install',
                'has_notice' => true,
                'dismissable' => false,
                'is_automatic' => true
        );
        tgmpa( $plugins, $configs );
 
}
add_action('tgmpa_register', 'thienpham_plugin_activation');
?>