<?php
/**
  @ Thiết lập các hằng dữ liệu quan trọng
  @ THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
  @ CORE = thư mục /core của theme, chứa các file nguồn quan trọng.
**/
define( 'THEME_URL', get_stylesheet_directory() );
define( 'CORE', THEME_URL . '/core' );

/**
  @ Load file /core/init.php
  @ Đây là file cấu hình ban đầu của theme mà sẽ không nên được thay đổi sau này.
**/
require_once( CORE . '/init.php' );

/**
 @ Thiết lập $content_width để khai báo kích thước chiều rộng của nội dung
**/
if ( ! isset( $content_width ) ) {
   /*
    * Nếu biến $content_width chưa có dữ liệu thì gán giá trị cho nó
    */
   $content_width = 620;
}

/**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
**/
if ( ! function_exists( 'thienpham_theme_setup' ) ) {
    /*
     * Nếu chưa có hàm thienpham_theme_setup() thì sẽ tạo mới hàm đó
     */
    function thienpham_theme_setup() {
    	/* Thiết lập textdomain */
    	$languages_folder = THEME_URL . '/languages';
    	load_theme_textdomain( 'thienpham' , $languages_folder );

    	/* Tự động thêm link RSS lên <head> */
    	add_theme_support(' automatic-feed-links ');

    	/* Thêm post thumbnail */
    	if (function_exists('add_theme_support')) {
		    add_theme_support('post-thumbnails');
		}

		/* Post Format */
		add_theme_support( 'post-formats',
		    array(
		       'image',
		       'video',
		       'gallery',
		       'quote',
		       'link'
		    )
		 );

		/* Them title-tag */
		add_theme_support( 'title-tag' );
		
		/* Them custom background */
		$default_background = array(
		   'default-color' => '#e8e8e8',
		);
		add_theme_support( 'custom-background', $default_background );

		/* Thêm menu */
		register_nav_menu( 'primary-menu', 'Primary Menu' );
    register_nav_menus( array(
    'landing-menu'   => __( 'Landing', 'thienpham' ),
    'main-menu'   => __( 'Top Menu', 'thienpham' ),
    'right-nav'   => __( 'Right Nav', 'thienpham' ),
    'social-menu'   => __( 'Social Menu', 'thienpham' )
  ) );


		/* Tao sidebar */
		$sidebar = array(
		   'name' => __('Main Sidebar', 'thienpham'),
		   'id' => 'main-sidebar',
		   'description' => 'Main sidebar for Thienpham theme',
		   'class' => 'main-sidebar',
		   'before_title' => '<h3 class="widgettitle">',
		   'after_title' => '</h3>'
		);
		register_sidebar( $sidebar );
    }
    add_action ( 'init', 'thienpham_theme_setup' );

    /* Ẩn thanh Admin sidebar */
    //add_filter( 'show_admin_bar', '__return_false' );
}

/*--------------
TEMPLATE FUNCTIONS */
if ( ! function_exists( 'thienpham_logo' ) ) {
  function thienpham_logo() {?>
    <div class="logo">
 
      <div class="site-name">
        <?php if ( is_home() ) {
          printf(
            '<h1><a href="%1$s" title="%2$s">%3$s</a></h1>',
            get_bloginfo( 'url' ),
            get_bloginfo( 'description' ),
            get_bloginfo( 'sitename' )
          );
        } else {
          printf(
            '<p><a href="%1$s" title="%2$s">%3$s</a></p>',
            get_bloginfo( 'url' ),
            get_bloginfo( 'description' ),
            get_bloginfo( 'sitename' )
          );
        } // endif ?>
      </div>
      <div class="site-description"><?php bloginfo( 'description' ); ?></div>
 
    </div>
  <?php }
}

/**
Thiet lap menu
**/
if ( ! function_exists( 'thienpham_menu' ) ) {
  function thienpham_menu( $slug ) {
    $thienpham_walker = new ThienPham_Nav_Walker;
    $menu = array(
      'theme_location' => $slug,
      'container' => 'nav',
      'container_class' => $slug,
      'walker' => $thienpham_walker
    );
    wp_nav_menu( $menu );
  }
}

if ( ! function_exists( 'wp_bootstrap_nav' ) ) {
  function wp_bootstrap_nav( $slug ) {
  /* Primary navigation */
    wp_nav_menu( array(
      'theme_location' => $slug, //Menu location của bạn
      'depth' => 3, //Số cấp menu đa cấp
      'container' => 'div', //Thẻ bao quanh cặp thẻ ul
      'container_class'=>'collapse navbar-collapse navbar-ex1-collapse', //class của thẻ bao quanh cặp thẻ ul
      'menu_class' => 'nav navbar-nav', //class của thẻ ul
      'walker' => new wp_bootstrap_navwalker()) //Cái này để nguyên, không thay đổi
      );

  }
}

/**
Hàm tạo phân trang đơn giản
**/
if ( ! function_exists( 'thienpham_pagination' ) ) {
  function thienpham_pagination() {
    /*
     * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
     */
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
      return '';
    }
  ?>
 
  <nav class="pagination" role="navigation">
    <?php if ( get_next_post_link() ) : ?>
      <div class="prev"><?php next_posts_link( __('Older Posts', 'thienpham') ); ?></div>
    <?php endif; ?>
 
    <?php if ( get_previous_post_link() ) : ?>
      <div class="next"><?php previous_posts_link( __('Newer Posts', 'thienpham') ); ?></div>
    <?php endif; ?>
 
  </nav><?php
  }
}

/**
Hàm hiển thị thumbnail
@ Ảnh thumbnail sẽ không được hiển thị trong trang single
@ Nhưng sẽ hiển thị trong single nếu post đó có format là Image
@ thienpham_thumbnail( $size )
**/
if ( ! function_exists( 'thienpham_thumbnail' ) ) {
  function thienpham_thumbnail( $size ) {
 
    // Chỉ hiển thumbnail với post không có mật khẩu
    if ( ! is_single() &&  has_post_thumbnail()  && ! post_password_required() || has_post_format( 'image' ) ) : ?>
      <figure class="post-thumbnail"><?php the_post_thumbnail( $size ); ?></figure><?php
    endif;
  }
}

/**
@ Hàm hiển thị tiêu đề của post trong .entry-header
@ Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
@ Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
@ thienpham_entry_header()
**/
if ( ! function_exists( 'thienpham_entry_header' ) ) {
  function thienpham_entry_header() {
    if ( is_single() ) : ?>
 
      <h1 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h1>
    <?php else : ?>
      <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h2><?php
 
    endif;
  }
}

/**
@ Hàm hiển thị thông tin của post (Post Meta)
@ thienpham_entry_meta()
**/
if( ! function_exists( 'thienpham_entry_meta' ) ) {
  function thienpham_entry_meta() {
    if ( ! is_page() ) :
      echo '<div class="entry-meta">';
 
        // Hiển thị tên tác giả, tên category và ngày tháng đăng bài
        printf( __('<span class="author">Posted by %1$s</span>', 'thienpham'),
          get_the_author() );
 
        printf( __('<span class="date-published"> at %1$s</span>', 'thienpham'),
          get_the_date() );
 
        printf( __('<span class="category"> in %1$s</span>', 'thienpham'),
          get_the_category_list( ', ' ) );
 
        // Hiển thị số đếm lượt bình luận
        if ( comments_open() ) :
          echo ' <span class="meta-reply">';
            comments_popup_link(
              __('Leave a comment', 'thienpham'),
              __('One comment', 'thienpham'),
              __('% comments', 'thienpham'),
              __('Read all comments', 'thienpham')
             );
          echo '</span>';
        endif;
      echo '</div>';
    endif;
  }
}

/*
 * Thêm chữ Read More vào excerpt
 */
function thienpham_readmore() {
  return '...<a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'thienpham') . '</a>';
}
add_filter( 'excerpt_more', 'thienpham_readmore' );
 
/**
Hàm hiển thị nội dung của post type
@ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
@ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
@ thienpham_entry_content()
**/
if ( ! function_exists( 'thienpham_entry_content' ) ) {
  function thienpham_entry_content() {
 
    if ( ! is_single() ) :
      the_excerpt();
    else :
      the_content();
 
      /*
       * Code hiển thị phân trang trong post type
       */
      $link_pages = array(
        'before' => __('<p>Page:', 'thienpham'),
        'after' => '</p>',
        'nextpagelink'     => __( 'Next page', 'thienpham' ),
        'previouspagelink' => __( 'Previous page', 'thienpham' )
      );
      wp_link_pages( $link_pages );
    endif;
 
  }
}

/**
@ Hàm hiển thị tag của post
@ thienpham_entry_tag()
**/
if ( ! function_exists( 'thienpham_entry_tag' ) ) {
  function thienpham_entry_tag() {
    if ( has_tag() ) :
      echo '<div class="entry-tag">';
      printf( __('Tagged in %1$s', 'thienpham'), get_the_tag_list( '', ', ' ) );
      echo '</div>';
    endif;
  }
}

/**
@ Chèn CSS và Javascript vào theme
@ sử dụng hook wp_enqueue_scripts() để hiển thị nó ra ngoài front-end
**/
function thienpham_styles() {
  /* Jquery */
  wp_register_script( 'jquery-js', get_template_directory_uri() . '/js/jquery-1.12.1.min.js', array('jquery') );
  wp_enqueue_script( 'jquery-js' );
  /*
  * Hàm get_stylesheet_uri() sẽ trả về giá trị dẫn đến file style.css của theme
  * Nếu sử dụng child theme, thì file style.css này vẫn load ra từ theme mẹ
  */
  wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', 'all' );
  wp_enqueue_style( 'main-style' );
  wp_register_style( 'reset-style', get_template_directory_uri() . '/reset.css', 'all' );
  wp_enqueue_style( 'reset-style' );

  /* WOW animate */
  wp_register_style( 'WOW-master-style', get_template_directory_uri() . '/WOW-master/css/libs/animate.css', 'all');
  wp_enqueue_style( 'WOW-master-style' );
  wp_register_script( 'WOW-master-js', get_template_directory_uri() . '/WOW-master/dist/wow.min.js', array('jquery') );
  wp_enqueue_script( 'WOW-master-js' );


  /* Superfish Menu*/
  // wp_register_style( 'superfish-css', get_template_directory_uri() . '/css/superfish.css', 'all' );
  // wp_enqueue_style( 'superfish-css' );
  // wp_register_script( 'superfish-js', get_template_directory_uri() . '/js/superfish.js', array('jquery') );
  // wp_enqueue_script( 'superfish-js' );

  /* Custom js */
  wp_register_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', array('jquery') );
  wp_enqueue_script( 'custom-js' );

  /* Bootstrap */
  wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', 'all' );
  wp_enqueue_style( 'bootstrap-css' );
  wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
  wp_enqueue_script( 'bootstrap-js' );

  

  /* Landing_01 template */
  if(is_page_template( 'templates/langding01.php' ) ){
  wp_register_style( 'landing01-css', get_template_directory_uri() . '/css/landing01.css', 'all' );
  wp_enqueue_style( 'landing01-css' );
  }
}
add_action( 'wp_enqueue_scripts', 'thienpham_styles' );