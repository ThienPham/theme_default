<!DOCTYPE html>
<html <?php language_attributes(); ?> />
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]> <html <?php language_attributes(); ?>> <![endif]-->
 
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmgp.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <?php wp_head(); ?>
</head>
 
<body <?php body_class(); ?> > <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
    <div id="container">
        <header id="header">
           <?php thienpham_logo(); ?>
           <!-- <?php thienpham_menu( 'primary-menu' ); ?> -->
            <nav class="navbar navbar-default" role="navigation">
                <?php wp_bootstrap_nav( 'primary-menu' ); ?>
           </nav>
        </header>